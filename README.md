# Desafio Go Lang

## Como usar

Clone o projeto:

```
git clone https://gitlab.com/jonatanjrss/desafio-golang.git .
```

Levante o container:

```
cd desafio-golang
docker-compose up
```

Acesse as urls:

http://localhost:8000/tasks

http://localhost:8000/tasks-api
