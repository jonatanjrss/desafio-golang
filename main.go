package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"text/template"
)

type task struct {
	Name string
	Done bool
}

var tasks []task

func main() {

	fmt.Println("Acesse http://localhost:8000/tasks ou http://localhost:8000/tasks-api")

	tasks = []task{
		{Name: "Tarefa 1", Done: false},
		{Name: "Tarefa 2", Done: true},
	}

	// cria as urls
	http.HandleFunc("/tasks", tasksHandler)
	http.HandleFunc("/tasks-api", tasksAPIHandler)

	// levanta o servidor
	http.ListenAndServe(":8887", nil)

}

func tasksHandler(w http.ResponseWriter, r *http.Request) {
	// retorna o template "tasks.html". O slice de bytes "tasks" foi passado como contexto.
	// Por isso é acessível no template "tasks.html"

	tmpl := template.Must(template.ParseFiles("tasks.html"))
	tmpl.Execute(w, tasks)

}

func tasksAPIHandler(w http.ResponseWriter, r *http.Request) {
	// retorna json

	tasksJSON, error := json.Marshal(tasks)

	if error != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(tasksJSON)

}
